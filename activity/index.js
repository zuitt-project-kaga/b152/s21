const assets = [
  {
    id: "A01",
    name: "toilet paper",
    description: "nice poop paper",
    stock: 54,
    isAvailable: true,
    dateAdded: "2019-08-21",
  },
  {
    id: "B02",
    name: "Gumi",
    description: "delicious",
    stock: 23,
    isAvailable: true,
    dateAdded: "2022-1-1",
  },
];

console.log(assets);

let jsonSample = `{
    "sampleKey1" : "valueA",
    "sampleKey2" : "valueB",
    "sampleKey3" : 1,
    "sampleKey4" : true
}`;

console.log(typeof jsonSample);

let jsonConvert = JSON.parse(jsonSample);
console.log(typeof jsonConvert);
console.log(jsonConvert);

let batches = [
  {
    batch: "Batch 152",
  },
  {
    batch: "Batch 156",
  },
];
let batchesJSON = JSON.stringify(batches);
console.log(batchesJSON);

let data = {
  name: "Katniss",
  age: 20,
  address: {
    city: "Kansan City",
    state: "Kansas",
  },
};
let dataJSON = JSON.stringify(data);
console.log(dataJSON);

let data2 = {
  username: "saitamaOPM",
  password: "onepuuuuunch",
  isAdmin: true,
};
let data3 = {
  username: "lightYamami",
  password: "notkiraDeninitely",
  isAdmin: false,
};
let data4 = {
  username: "Llawliet",
  password: "yagamiikiraa07",
  isAdmin: false,
};

let data2JSON = JSON.stringify(data2);
let data3JSON = JSON.stringify(data3);
let data4JSON = JSON.stringify(data4);

console.log(data2JSON);
console.log(data3JSON);
console.log(data4JSON);

let jsonArray = JSON.stringify(assets);

console.log(jsonArray);

let items = `[
    {
        "id":"shop-1",
        "name":"Oreos",
        "stock":5,
        "price":50
    },
    {
        "id":"shop-2",
        "name":"Doritos",
        "stock":10,
        "price":150
    }
]`;

let itemsJSArr = JSON.parse(items);
console.log(itemsJSArr);

itemsJSArr.pop();
console.log(itemsJSArr);

items = JSON.stringify(itemsJSArr);
console.log(items);

let courses = `[
    {
        "name":"Math 101",
        "description":"learn the basics of Math",
        "price":2500
    },
    {
        "name":"Science 102",
        "description":"learn the basics of Science",
        "price":2500
    }
]`;

let coursesJSArr = JSON.parse(courses);
coursesJSArr.pop();
courses = JSON.stringify(coursesJSArr);
console.log(courses);

let coursesJSArr2 = JSON.parse(courses);
coursesJSArr2.push({
  name: "Hitsory 301",
  description: "learn the basics of Math",
  price: 1100000000,
});
courses = JSON.stringify(coursesJSArr2);

console.log(courses);